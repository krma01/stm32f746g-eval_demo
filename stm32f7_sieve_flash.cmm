;-----------------------------------------------------------------------------------
; @Title: Simple sieve demo for STM32F7xx derivatives with Flash programming
; @Description:
;   This is a small demo how to setup a SWD debug session on Cortex-M7
;   for STM32F7xx derivatives. The sieve demo is loaded on FLASH.
;
;   Prerequisites:
;    * Combiprobe is connected to the MIPI20T TRACE connector
;      OR
;      DebugCable is connected to the CN15 JTAG connector
;
; @Keywords: Cortex-M7, debug,STM32, SWD
; @Author: AME
; @Board: STM32F756G-Eval
; @Chip: STM32F7*
; @Copyright: (C) 1989-2014 Lauterbach GmbH, licensed for use with TRACE32(R) only
; --------------------------------------------------------------------------------
; $Id: stm32f7_sieve_flash.cmm 8767 2015-10-08 09:37:19Z amerkle $

; Basic setup
RESet
SYStem.RESet
SYStem.CPU STM32F746ZG
SYStem.CONFIG.DEBUGPORTTYPE SWD
SYStem.CONFIG.CONNECTOR MIPI20T
SYStem.MemAccess DAP
SYStem.Option DUALPORT ON
SYStem.Up

; prepare flash programming
DO ~~/demo/arm/flash/stm32f7xx PREPAREONLY

; load example code
FLASH.ReProgram ALL
Data.LOAD.auto ./Debug/Blinky.axf
FLASH.ReProgram OFF

; reset the target again
SYStem.Up

;disable Watchdog
Data.Set C:0x40002C00  %Long Data.Long(C:0x40002C00)&~0x80

; run till main
Go.direct main
WAIT !STATE.RUN()

; open some windows
Mode.Hll
List.auto